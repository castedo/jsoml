all:
	@echo -e "Nope, try:\n  make test"

test:
	mypy jsoml
	python -m pytest
	@echo Done

.PHONY: all test
